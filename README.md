# Frontside Application

Frontside is a simple client for the Backside application. It's been created for demonstration purposes
so it's just a simple project where some rigorous aspects have been relaxed in order to keep the solution
simple and clearly show the main guidelines of a concurrent design.

The goal is to call the Backside application from different threads running concurrently. It's
actually a stressing tool.

The logic is quite simple. A given number of threads keep making request to the backend in
an infinite basis. The resulting body is then printed on the stdout.

Please, notice that no synchronization has been set for printing
the result as it was intended to keep the maximum level of parallelism. Some kind of
synchronization in the form of either a lock or a separated collecting thread would imply
serialization. Not synchronizing the output might result in interleaved output, fact that
it's ok for our concurrency compromise and also fun (as we see that we're dealing with an
actual concurrent and parallel system (parallel if you run this app on a multicore
machine... of course you do)).

Running it this way:

```
$  gradle build && gradle jar && java -jar build/libs/frontside.jar
```

Have fun!!!
