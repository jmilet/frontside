import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.time.Duration;
import java.util.Optional;

public class Main {
    public static void main(String[] argv) throws IOException {
        final int numClients = 10;
        final String url = "http://localhost:8080/job";
        final String json = "{\"name\":\"uno\", \"description\": \"A desciption\"}";

        for (int i = 0; i < numClients; i++) {
            runClient(i, url, json);
        }

        sleepForever();
    }

    /**
     * Creates and run a new client on its own thread.
     * @param clientId The client id representing this thread.
     * @param url The url of the endpoint.
     * @param json The json payload.
     * @throws IOException
     */
    private static void runClient(final int clientId, final String url, final String json) throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);

        StringEntity entity = new StringEntity(json);
        httpPost.setEntity(entity);
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");

        new Thread(() -> {
            while (true) {
                Optional<String> body = callAndGetResponse(client, httpPost);

                if (body.isPresent()) {
                    System.out.println(String.format("Client %d got body %s", clientId, body));
                }
                else {
                    System.out.println(String.format("Client %d failed to call the endpoint", clientId));
                    sleepFor(Duration.ofSeconds(10));
                }
            }
        }).start();
    }

    /**
     * Executes a POST request on the given client. This method is pure in the sense that it doesn't throw
     * any kind of exception.
     * @param client The client to use in the call.
     * @param httpPost The object to post.
     * @return An Optional of String representing the body. It's empty in case of error.
     */
    private static Optional<String> callAndGetResponse(CloseableHttpClient client, HttpPost httpPost) {
        try (CloseableHttpResponse response = client.execute(httpPost)) {

            if (response.getStatusLine().getStatusCode() != 201) {
                return Optional.empty();
            }

            String responseBody = EntityUtils.toString(response.getEntity());
            return Optional.of(responseBody);
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    private static void sleepForever() {
        // We catch the exception as this is an example and we don't want the calling code the have to
        // mess with this exception.
       while (true) {
           try {
               Thread.sleep(Long.MAX_VALUE);
           } catch (InterruptedException e) {
               e.printStackTrace();
           }
       }
    }

    private static void sleepFor(final Duration duration) {
        // We catch the exception as this is an example and we don't want the calling code the have to
        // mess with this exception.
        try {
            Thread.sleep(duration.getSeconds() * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}